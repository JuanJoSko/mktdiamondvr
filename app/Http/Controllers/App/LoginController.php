<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Validator;
use Auth;


class LoginController extends Controller
{

    public function Login(Request $request)
    {
      $validator = Validator::make($request->all(),[
        'email'=>'required|email',
        'password'=>'required|min:6'
      ]);
      if($validator->fails()){
        return response()->json(['status'=>false,'errors'=>$validator->errors()->all()]);
      }else{
        if(Auth::attempt($request->all())){
          return response()->json(['status'=>true,'msg'=>['Usuario logeado'],'user'=>Auth::user()]);
        }else{
		return response()->json([
			'status'=>false,
            'msj' => "Correo o contraseña incorrectos.",
            'info' => "por favor verifique sus datos"
            ]);
        }
      }
    }
   
}
